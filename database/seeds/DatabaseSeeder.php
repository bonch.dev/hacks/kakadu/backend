<?php

use App\Models\Company;
use App\Models\Test\TestQuestion;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $adminCompany = Company::create([
            'name' => 'bonch.dev',
        ]);

        $adminCompany->managers()->create([
            'login' => 'admin',
            'email' => 'admin',
            'password' => bcrypt('admin'),
            'email_verified_at' => Carbon::now(),
        ]);

        factory(Company::class, 10)->create();

        factory(User::class, 1)->create([
            'company_id' => 1,
            'first_name' => 'Господин',
            'last_name' => 'Торквемада',
            'gender' => 'male',
            'phone' => '+79110297197',
        ]);

        /** @var Company $company */
        foreach (Company::all() as $company) {
            if ($company->id == 1) continue;
            $company->users()->createMany(
                factory(User::class, rand(2, 10))
                    ->make(['company_id' => $company->id])->toArray()
            );
        }

        $test = factory(\App\Models\Test\Test::class, 1)->create()->first();
        for ($i = 1; $i <= 10; $i++) {
            factory(TestQuestion::class, 1)->create([
                'test_id' => $test->id,
                'number' => $i,
            ]);
        }
    }
}
