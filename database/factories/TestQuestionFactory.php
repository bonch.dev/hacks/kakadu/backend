<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Test\TestQuestion;
use Faker\Generator as Faker;

$factory->define(TestQuestion::class, function (Faker $faker) {
    return [
        'text' => $faker->text,
        'type' => $faker->randomElement(['number', 'string'])
    ];
});
