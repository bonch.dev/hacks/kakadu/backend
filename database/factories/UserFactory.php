<?php

use App\Models\User\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$genders = ['male', 'female'];

$factory->define(User::class, function (Faker $faker) use ($genders) {
    $gender = $genders[rand(0, 1)];
    return [
        'first_name' => $faker->firstName($gender),
        'last_name' => $faker->lastName,
        'gender' => $gender,
        'department' => $faker->company,
        'comment' => $faker->text,
        'phone' => $faker->phoneNumber,
        'INN' => $faker->unique()->numerify('########'),
        'vk_link' => $faker->randomElement([
            'https://vk.com/nevmetovazat',
            'https://vk.com/zzer0z',
            'https://vk.com/beatdees',
            'https://vk.com/id158320807',
            '1nform_bonch',
            'xcycles',
        ]),
        'headhunter_link' => $faker->randomElement([
            'https://spb.hh.ru/resume/30d02f3a0006087c550039ed1f4f3545384370',
            'https://spb.hh.ru/resume/a619d3170005e9b62b0039ed1f355665736b39',
            'https://spb.hh.ru/resume/37f4a0730005d84e4d0039ed1f6f774f325370',
            'https://spb.hh.ru/resume/fe1ee8690006ff257d0039ed1f314c725a4d46',
            'https://spb.hh.ru/resume/1d5b05180005b6a7d90039ed1f4d3177467368',
            'https://spb.hh.ru/resume/4e8dea1900053621ee0039ed1f5344576f5059',
        ]),
    ];
});
