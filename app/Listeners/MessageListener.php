<?php

namespace App\Listeners;

use App\Events\MessageCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use \GuzzleHttp\Client as Guzzle;
use Telegram\Bot\Laravel\Facades\Telegram;

class MessageListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param MessageCreated $event
     * @return void
     */
    public function messageCreatedHandle(MessageCreated $event)
    {
        $message = $event->message;

        $text = "Привет, {$message->user->first_name}!\n" .
                "Твой манагер оставил для тебя обратную связь, прочитай, пожалуйста!" .
                "\n\n{$message->text}";

        if ($telegram_id = $message->user->telegram->username) {
            \Telegram::sendMessage([
                'chat_id' => $telegram_id,
                'text' => $text,
            ]);
        }
    }
}
