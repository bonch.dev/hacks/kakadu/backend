<?php

namespace App\Listeners;

use App\Events\TestProgressCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Telegram;

class TestProgressListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param TestProgressCreated $event
     * @return void
     */
    public function testProgressCreatedHandle(TestProgressCreated $event)
    {
        $testProgress = $event->testProgress;

        if ($telegramId = $testProgress->user->telegram->username) {
            Telegram::sendMessage([
                'chat_id' => $telegramId,
                'text' => "Вам был назначен новый тест ({$testProgress->test->name})"
            ]);
            $testProgress->sendNextQuestion();
        }
    }
}
