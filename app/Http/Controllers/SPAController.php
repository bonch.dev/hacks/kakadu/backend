<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class SPAController
 * @package App\Http\Controllers\Site
 */
class SPAController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('SPA');
    }
}
