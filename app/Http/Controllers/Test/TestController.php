<?php

namespace App\Http\Controllers\Test;

use App\Http\Requests\StartTestToUsersRequest;
use App\Http\Requests\StoreTestRequest;
use App\Http\Requests\UpdateTestRequest;
use App\Models\Test\Test;
use App\Models\Test\TestProgress;
use App\Models\User\User;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Test::paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Test|Model
     */
    public function store(StoreTestRequest $request)
    {
        $test = Test::create($request->toArray());

        $test->company_id = auth()->user()->company_id;

        $test->questions()->createMany($request->questions);

        $test->save();

        return $test;
    }

    /**
     * Display the specified resource.
     *
     * @param Test $test
     * @return Response
     */
    public function show(Test $test)
    {
        return response()->json($test);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTestRequest $request
     * @param Test $test
     * @return Test
     */
    public function update(UpdateTestRequest $request, Test $test)
    {
        $test->company_id = auth()->user()->company_id;

        $test->questions()->delete();

        $test->questions()->createMany($request->questions);

        $test->save();

        return $test;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Test $test
     * @return JsonResponse
     */
    public function destroy(Test $test)
    {
        try {
            $test->delete();
        } catch (Exception $exception) {
            return response()
                ->json([
                    "success" => false,
                    "exception" => $exception
                ])->setStatusCode(500);
        }
        return response()->json(["success" => true]);
    }

    public function startToUser(Test $test, User $user)
    {
        /** @var TestProgress $progress */
        $progress = TestProgress::make();

        $progress->user()->associate($user);
        $progress->test()->associate($test);
        $progress->currentQuestion()->associate($test->questions()->first());

        $progress->save();

        return response()->json(['success' => true]);
    }

    public function startToUsers(StartTestToUsersRequest $request, Test $test)
    {
        foreach ($request->users as $user_id) {
            $user = User::find($user_id);

            /** @var TestProgress $progress */
            $progress = TestProgress::make();

            $progress->user()->associate($user);
            $progress->test()->associate($test);
            $progress->currentQuestion()->associate($test->questions()->first());

            $progress->save();
        }

        return response()->json(['success' => true]);
    }
}
