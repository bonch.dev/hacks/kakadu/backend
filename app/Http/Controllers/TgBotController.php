<?php

namespace App\Http\Controllers;

use App\Models\User\User;
use App\Models\User\UserTelegram;
use Illuminate\Http\Request;
use \Telegram;

/**
 * @property Request request
 * @property array funtale
 */
class TgBotController extends Controller
{
    public function __construct()
    {
        $this->funtale = [
            "xxx: Да ладно, ты же такой умный и добрый, ты обязательно найдешь себе девушку!\n" .
            "yyy: Должно быть это правда, потому что я часто это слышу.\n".
            "yyy: Например, все девушки, за которыми я пытался ухаживать, говорили мне то же самое ((( ",

            "Я опежаю своих сверстников в развитии. Пока все заводят семьи и рожают детей, я уже старый больной дед, который всем недоволен. ",

            "xxx: Считаю нужным напомнить молодой шпане, что принтеры brother это не только браззерс, но и вротхер. ",

            "обсуждение финала ИП:\n" .
            "ххх: Во всем этом дурдоме лучше всего дракон канешн смотрелся. Сжег трон и улетел, жаль не насрал еще в тронном зале. "
        ];
    }

    public function index(Request $request)
    {
        $this->request = $request;

        $userId = $request->input('message.from.id');
        switch ($request->input('message.text')) {
            case "/start":
                $this->commandStart($userId);
                break;
            case "/info":
                \Telegram::sendMessage([
                    'chat_id' => $userId,
                    'text' => "Привет!\n" .
                              "Kakadu - сервис улучшения обратной связи внутри творческих команд!\n".
                              "Наша цель - упростить коммуникацию внутри больших и маленьких команд, " .
                              "сплотить их перед большим фронтом задач, и просто сделать жизнь чуточку проще и светлее!\n\n" .
                              "С ❤, Bonch.dev!"
                ]);
                break;
            default:
                $this->stateAction($userId);
                break;
        }
        return response()->json(true);
    }

    private function commandStart(int $userId)
    {
        $tgUser = UserTelegram::where('username', $userId)->first();
        if (!$tgUser) {
            UserTelegram::create(['username' => $userId, 'state' => 'register']);
            \Telegram::sendMessage([
                'chat_id' => $userId,
                'text' => 'Для начала работы введи свой номер телефона'
            ]);
        } else {
            if ($tgUser->user()->exists()) {
                \Telegram::sendMessage([
                    'chat_id' => $userId,
                    'text' => 'Мы уже знакомы, и скоро продолжим работу =)'
                ]);
            } else {
                $tgUser->state = 'register';
                $tgUser->save();
                \Telegram::sendMessage([
                    'chat_id' => $userId,
                    'text' => 'Для начала работы введи свой номер телефона'
                ]);
            }
        }
    }

    private function stateAction(int $userId)
    {
        /** @var UserTelegram $tgUser */
        $tgUser = UserTelegram::where('username', $userId)->first();
        if ($tgUser) {
            switch ($tgUser->state) {
                case "register":
                    $phoneNumber = $this->request->input('message.text');
                    $user = User::wherePhone($phoneNumber)->first();
                    if ($user) {
                        $tgUser->user()->associate($user);
                        $tgUser->state = "idle";
                        $tgUser->save();

                        \Telegram::sendMessage([
                            'chat_id' => $userId,
                            'text' => "Привет, {$user->name}!\nМы тебя знаем, и ты теперь часть нашего дружного коллектива =)"
                        ]);
                    } else {
                        \Telegram::sendMessage([
                            'chat_id' => $userId,
                            'text' => "Прости, пожалуйста, но, похоже, тебя нет в списке, а я боюсь разговаривать с незнакомцами."
                        ]);
                    }
                    break;
                case "idle":
                    \Telegram::sendMessage([
                        'chat_id' => $userId,
                        'text' => "Тебе скучно? Давай, я расскажу тебе анекдот:\n\n" .
                            $this->funtale[rand(0, count($this->funtale))]
                    ]);
                    break;
                case "question":
                    $tgUser->user->test->answer($this->request->input('message.text'));
                    break;
            }
        } else {
            \Telegram::sendMessage([
                'chat_id' => $userId,
                'text' => "Прости, но я тебя не знаю, и не понимаю... "
            ]);
        }
    }
}
