<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if (!$token = auth()->attempt([
            'email' => $request->login,
            'password' => $request->password
        ], true)) {

            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = Auth::user();

        return response()->json( array_merge(
            $user->toArray(),
            [
                'token' => $token,
                'expires_in' => auth('api')->factory()->getTTL() * 60,
            ]
        ));

    }

    public function logout(Request $request)
    {
        Auth::logout();
        return response()->json(true);
    }

    public function check()
    {
        return response()->json(auth('api')->user());
    }
}
