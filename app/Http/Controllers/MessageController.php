<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMessageRequest;
use App\Models\Message;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return LengthAwarePaginator|Response
     */
    public function index()
    {
        return Message::whereManagerId(auth()->id())->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreMessageRequest $request
     * @return Message|Model
     */
    public function store(StoreMessageRequest $request)
    {
        $message = Message::create($request->all());

        $message->manager()->associate(auth()->user());

        return $message;
    }

    /**
     * Display the specified resource.
     *
     * @param Message $message
     * @return Message
     */
    public function show(Message $message)
    {
        return $message;
    }

    /**
     * Update the specified resource in storage.
     *
     * @return Response
     */
    public function update()
    {
        return response()
            ->json(['success' => false, 'exception' => 'You cannot update a message'])
            ->setStatusCode(401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy()
    {
        return response()
            ->json(['success' => false, 'exception' => 'You cannot delete a message'])
            ->setStatusCode(401);
    }
}
