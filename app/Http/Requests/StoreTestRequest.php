<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Nexmo\Call\Collection;

/**
 * @property string name
 * @property string class
 * @property Collection questions
 */
class StoreTestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'class' => 'required|string',

            'questions' => 'required|array',
            'questions.*.text' => 'required|string',
            'questions.*.type' => [
                'required',
                Rule::in(['number', 'string'])
            ],
            'questions.*.number' => 'required|numeric'
        ];
    }
}
