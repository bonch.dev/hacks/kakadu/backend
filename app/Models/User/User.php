<?php

namespace App\Models\User;

use App\Models\Test\TestProgress;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Support\Carbon;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\Models\User\User
 *
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @method static bool|null forceDelete()
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static Builder|User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $department
 * @property string|null $comment
 * @property string|null $phone
 * @property string|null $INN
 * @property string|null $vk_link
 * @property string|null $headhunter_link
 * @property int|null $company_id
 * @property string|null $telegram_id
 * @method static Builder|User whereComment($value)
 * @method static Builder|User whereCompanyId($value)
 * @method static Builder|User whereDepartment($value)
 * @method static Builder|User whereFirstName($value)
 * @method static Builder|User whereHeadhunterLink($value)
 * @method static Builder|User whereINN($value)
 * @method static Builder|User whereLastName($value)
 * @method static Builder|User wherePhone($value)
 * @method static Builder|User whereTelegramId($value)
 * @method static Builder|User whereVkLink($value)
 * @property string $gender
 * @method static Builder|User whereGender($value)
 * @property string|null $telegram_state
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereTelegramState($value)
 * @property-read \App\Models\User\UserTelegram $telegram
 * @property-read \App\Models\Test\TestProgress $test
 */
class User extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    public function test()
    {
        return $this->hasOne(TestProgress::class);
    }

    public function telegram()
    {
        return $this->hasOne(UserTelegram::class);
    }

    public function getNameAttribute()
    {
        return "$this->first_name $this->last_name";
    }
}
