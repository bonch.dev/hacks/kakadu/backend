<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * App\Models\User\Manager
 *
 * @property int $id
 * @property string $login
 * @property string|null $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property int|null $company_id
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Manager newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Manager newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User\Manager onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Manager query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Manager whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Manager whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Manager whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Manager whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Manager whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Manager whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Manager whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Manager wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Manager whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Manager whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User\Manager withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User\Manager withoutTrashed()
 * @mixin \Eloquent
 */
class Manager extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    protected $fillable = [
        'name', 'email', 'password', 'phone',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
