<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\UserWorkPlace
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $work_time
 * @property string|null $company
 * @property string|null $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkPlace newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkPlace newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkPlace query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkPlace whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkPlace whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkPlace whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkPlace whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkPlace wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkPlace whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkPlace whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkPlace whereWorkTime($value)
 * @mixin \Eloquent
 */
class UserWorkPlace extends Model
{
    //
}
