<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\UserTelegram
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $username
 * @property string|null $state
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserTelegram newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserTelegram newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserTelegram query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserTelegram whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserTelegram whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserTelegram whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserTelegram whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserTelegram whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserTelegram whereUsername($value)
 * @mixin \Eloquent
 */
class UserTelegram extends Model
{
    protected $fillable = [
        'username',
        'state',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
