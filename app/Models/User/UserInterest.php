<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\UserInterest
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property float $percents
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserInterest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserInterest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserInterest query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserInterest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserInterest whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserInterest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserInterest whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserInterest wherePercents($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserInterest whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserInterest whereUserId($value)
 * @mixin \Eloquent
 */
class UserInterest extends Model
{
    //
}
