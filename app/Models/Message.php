<?php

namespace App\Models;

use App\Events\MessageCreated;
use App\Models\User\Manager;
use App\Models\User\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Message
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $manager_id
 * @property string|null $text
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read mixed $is_sended
 * @property-read Manager|null $manager
 * @property-read User|null $user
 * @method static Builder|Message newModelQuery()
 * @method static Builder|Message newQuery()
 * @method static Builder|Message query()
 * @method static Builder|Message whereCreatedAt($value)
 * @method static Builder|Message whereId($value)
 * @method static Builder|Message whereManagerId($value)
 * @method static Builder|Message whereText($value)
 * @method static Builder|Message whereUpdatedAt($value)
 * @method static Builder|Message whereUserId($value)
 * @mixin Eloquent
 * @property int|null $sended_at
 * @method static Builder|Message whereSendedAt($value)
 */
class Message extends Model
{
    protected $fillable = [
        'user_id',
        'manager_id',
        'text',
    ];

    protected $casts = [
        'sended_at' => 'timestamp'
    ];

    protected $dispatchesEvents = [
        'created' => MessageCreated::class
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function manager()
    {
        return $this->belongsTo(Manager::class);
    }

    public function getIsSendedAttribute()
    {
        return $this->sended_at == null;
    }
}
