<?php

namespace App\Models\Test;

use App\Models\User\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Telegram;

/**
 * App\Models\Test\TestProgress
 *
 * @property int $id
 * @property int|null $test_id
 * @property int|null $user_id
 * @property int|null $current_question_id
 * @property int $completed
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static Builder|TestProgress newModelQuery()
 * @method static Builder|TestProgress newQuery()
 * @method static Builder|TestProgress query()
 * @method static Builder|TestProgress whereCompleted($value)
 * @method static Builder|TestProgress whereCreatedAt($value)
 * @method static Builder|TestProgress whereCurrentQuestionId($value)
 * @method static Builder|TestProgress whereDeletedAt($value)
 * @method static Builder|TestProgress whereId($value)
 * @method static Builder|TestProgress whereTestId($value)
 * @method static Builder|TestProgress whereUpdatedAt($value)
 * @method static Builder|TestProgress whereUserId($value)
 * @mixin Eloquent
 * @property-read TestQuestion|null $currentQuestion
 * @property-read Test|null $test
 * @property-read User|null $user
 */
class TestProgress extends Model
{
    protected $fillable = [
        'user_id',
        'test_id',
        'current_question_id',
    ];

    protected $dispatchesEvents = [
        'created'
    ];

    //
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function test()
    {
        return $this->belongsTo(Test::class);
    }

    public function currentQuestion()
    {
        return $this->belongsTo(TestQuestion::class, 'current_question_id');
    }

    public function sendNextQuestion()
    {
        $text = "Вопрос " .
                "{$this->currentQuestion->number} из " .
                "{$this->test->questions()->orderByDesc('number')->first()->number}:\n\n" .
                $this->currentQuestion->text;

        $reply_markup = '';

        switch ($this->currentQuestion->type) {
            case "number":
                $keyboard = [
                    [
                        ["text" => "1"],
                        ["text" => "2"],
                        ["text" => "3"],
                        ["text" => "4"],
                        ["text" => "5"],
                    ]
                ];
                $reply_markup = Telegram::replyKeyboardMarkup([
                    'keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);
                break;
            case "string":
                $text .= "\n\nВведите ваш ответ в поле:";
                $reply_markup = Telegram::replyKeyboardHide();
                break;
        }

        $this->user->telegram->setAttribute('state', 'question');

        Telegram::sendMessage([
            'chat_id' => $this->user->telegram->username,
            'text' => $text,
            'reply_markup' => $reply_markup,
        ]);
    }

    public function answer($text)
    {
        /** @var QuestionAnswer $answer */
        $answer = QuestionAnswer::make(['text' => $text]);
        $answer->user()->associate($this->user);
        $answer->question()->associate($this->currentQuestion);
        $answer->save();

        if ($this->test->questions()->orderByDesc('number')->first()->number === $this->currentQuestion->number) {
            $this->setAttribute('completed', true);
            $reply_markup = Telegram::replyKeyboardHide();

            Telegram::sendMessage([
                'chat_id' => $this->user->telegram->username,
                'text' => "Спасибо за прохождение теста!",
                'reply_markup' => $reply_markup,
            ]);

            $this->user->telegram->setAttribute('state', 'idle');
        } else {
            $this->currentQuestion()->associate(
                $this->test
                    ->questions()
                    ->where('number', '>', $this->currentQuestion->number)
                    ->orderBy('number')
                    ->first()
            )->save();
            $this->sendNextQuestion();
        }
    }
}
