<?php

namespace App\Models\Test;

use App\Models\Company;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Test\Test
 *
 * @property int $id
 * @property string $name
 * @property int|null $company_id
 * @property string $class
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\Test newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\Test newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\Test query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\Test whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\Test whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\Test whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\Test whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\Test whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\Test whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\Test whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Test\TestQuestion[] $questions
 * @property-read \App\Models\Company|null $company
 */
class Test extends Model
{
    protected $fillable = [
        'name',
        'class',
    ];

    public function questions()
    {
        return $this->hasMany(TestQuestion::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
