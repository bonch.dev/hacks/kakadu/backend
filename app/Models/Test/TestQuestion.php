<?php

namespace App\Models\Test;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Models\Test\TestQuestion
 *
 * @property int $id
 * @property int|null $test_id
 * @property string $text
 * @property string $type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static Builder|TestQuestion newModelQuery()
 * @method static Builder|TestQuestion newQuery()
 * @method static Builder|TestQuestion query()
 * @method static Builder|TestQuestion whereCreatedAt($value)
 * @method static Builder|TestQuestion whereDeletedAt($value)
 * @method static Builder|TestQuestion whereId($value)
 * @method static Builder|TestQuestion whereTestId($value)
 * @method static Builder|TestQuestion whereText($value)
 * @method static Builder|TestQuestion whereType($value)
 * @method static Builder|TestQuestion whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Test|null $test
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|TestQuestion onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|TestQuestion withTrashed()
 * @method static \Illuminate\Database\Query\Builder|TestQuestion withoutTrashed()
 * @property int $number
 * @method static Builder|TestQuestion whereNumber($value)
 */
class TestQuestion extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'text',
        'type',
        'number',
    ];

    public function test()
    {
        return $this->belongsTo(Test::class);
    }
}
