<?php

namespace App\Models\Test;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Test\QuestionAnswer
 *
 * @property int $id
 * @property int|null $question_id
 * @property int|null $user_id
 * @property string $text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\QuestionAnswer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\QuestionAnswer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\QuestionAnswer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\QuestionAnswer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\QuestionAnswer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\QuestionAnswer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\QuestionAnswer whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\QuestionAnswer whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\QuestionAnswer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test\QuestionAnswer whereUserId($value)
 * @mixin \Eloquent
 */
class QuestionAnswer extends Model
{
    use SoftDeletes;

    protected $fillable = ['text'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function question()
    {
        return $this->belongsTo(TestQuestion::class, 'question_id');
    }
}
