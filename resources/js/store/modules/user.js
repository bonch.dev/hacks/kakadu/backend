import { USER_REQUEST, USER_ERROR, USER_SUCCESS } from '../actions/user'
import Vue from 'vue'
import axios from 'axios';
import { AUTH_LOGOUT } from '../actions/auth'

import {
    LOAD_USERS_ERROR,
    LOAD_USERS_REQUEST,
    LOAD_USERS_SUCCESS,

    CREATE_USER_ERROR,
    CREATE_USER_REQUEST,
    CREATE_USER_SUCCESS,

    UPDATE_USER_ERROR,
    UPDATE_USER_REQUEST,
    UPDATE_USER_SUCCESS,

    DELETE_USER_ERROR,
    DELETE_USER_REQUEST,
    DELETE_USER_SUCCESS
} from "../actions/user";

const state = {
    status: '',
    profile: {},
    users: [],
};

const getters = {
    getProfile: state => state.profile,
    isProfileLoaded: state => !!state.profile.name,
    users: state => state.users,
};

const actions = {
    [USER_REQUEST]: ({commit, dispatch}) => {
        commit(USER_REQUEST);
        axios.get('/api/user/me')
            .then(resp => {
                commit(USER_SUCCESS, resp.data)
            })
            .catch(() => {
                commit(USER_ERROR);
                dispatch(AUTH_LOGOUT)
            })
    },
    [LOAD_USERS_REQUEST]: ({commit}) => {
        return new Promise((resolve, reject) => {
            commit(LOAD_USERS_REQUEST);
            axios.get('/api/user')
                .then((resp) => {
                    let data = resp.data;
                    commit(LOAD_USERS_SUCCESS, data);
                    resolve(resp);
                })
                .catch((resp) => {
                    commit(LOAD_USERS_ERROR);
                    reject(resp.response.data);
                })
        })
    },
    [CREATE_USER_REQUEST]: ({commit}, user) => {
        return new Promise((resolve, reject) => {
            commit(CREATE_USER_REQUEST);
            axios.post('/api/user', user)
                .then((resp) => {
                    commit(CREATE_USER_SUCCESS, resp.data);
                    resolve(resp);
                })
                .catch((resp) => {
                    commit(CREATE_USER_ERROR);
                    reject(resp.response.data);
                })
        })
    },
    [UPDATE_USER_REQUEST]: ({commit}, user) => {
        return new Promise((resolve, reject) => {
            commit(UPDATE_USER_REQUEST);
            axios.patch('/api/user/' + user.id, user)
                .then((resp) => {
                    commit(UPDATE_USER_SUCCESS, resp.data);
                    resolve(resp);
                })
                .catch((resp) => {
                    commit(UPDATE_USER_ERROR);
                    reject(resp.response.data);
                })
        })
    },
    [DELETE_USER_REQUEST]: ({commit}, userId) => {
        return new Promise((resolve, reject) => {
            commit(DELETE_USER_REQUEST);
            axios.delete('/api/user/' + userId)
                .then((resp) => {
                    commit(DELETE_USER_SUCCESS, userId);
                    resolve(resp);
                })
                .catch((resp) => {
                    commit(DELETE_USER_ERROR);
                    reject(resp.response.data);
                })
        })
    },
};

const mutations = {
    [USER_REQUEST]: (state) => {
        state.status = 'loading'
    },
    [USER_SUCCESS]: (state, data) => {
        state.status = 'success';
        Vue.set(state, 'profile', data);
    },
    [USER_ERROR]: (state) => {
        state.status = 'error'
    },

    [LOAD_USERS_REQUEST]: (state) => {
        state.status = 'loading'
    },
    [LOAD_USERS_SUCCESS]: (state, data) => {
        state.status = 'success';
        state.users = data;
    },
    [LOAD_USERS_ERROR]: (state) => {
        state.status = 'error';
    },

    [CREATE_USER_REQUEST]: (state) => {
        state.status = 'loading'
    },
    [CREATE_USER_SUCCESS]: (state, user) => {
        state.status = 'success';
        state.users.push(user);
    },
    [CREATE_USER_ERROR]: (state) => {
        state.status = 'error';
    },

    [UPDATE_USER_REQUEST]: (state) => {
        state.status = 'loading'
    },
    [UPDATE_USER_SUCCESS]: (state, user) => {
        state.status = 'success';
        let index = state.users.findIndex((elem) => elem.id === user.id);
        Vue.set(state.users, index, user);
    },
    [UPDATE_USER_ERROR]: (state) => {
        state.status = 'error';
    },

    [DELETE_USER_REQUEST]: (state) => {
        state.status = 'loading'
    },
    [DELETE_USER_SUCCESS]: (state, userId) => {
        state.status = 'success';
        let index = state.users.findIndex((elem) => elem.id === userId);
        Vue.delete(state.users, index);
    },
    [DELETE_USER_ERROR]: (state) => {
        state.status = 'error';
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};