import Vue from 'vue'
import App from './App.vue'
import VueEvents from 'vue-events'
import axios from 'axios';
import moment from 'moment'
import _ from 'lodash'
import $ from 'jquery'

require('bootstrap');

Vue.config.productionTip = false;

Vue.use(VueEvents);

window.axios = axios;
window.moment = moment;
window._ = _;
window.$ = $;

Vue.component('App', App);

const authToken = localStorage.getItem('user-token');

axios.defaults.headers.common['Authorization'] = 'Bearer ' + authToken;
