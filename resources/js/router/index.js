import Vue from 'vue'
import Router from 'vue-router'

import Home from '../views/Home.vue'
import Managment from '../views/Managment.vue'
import Staff from '../views/Staff.vue'
import Patterns from '../views/Patterns.vue'
import Editor from '../views/Editor.vue'
import Login from '../views/login.vue'
import store from '../store'

Vue.use(Router);

function availableRedirect() {
    if (!store.getters.isAuthenticated)
        return '/login';

    return '/management';
}

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/',
            name: 'home',
            component: Home,
            redirect: () => availableRedirect(),
            children: [
                {
                    path: '/management',
                    name: 'management',
                    component: Managment
                },
                {
                    path: '/staff',
                    name: 'staff',
                    component: Staff
                },
                {
                    path: '/patterns',
                    name: 'patterns',
                    component: Patterns
                },
                {
                    path: '/editor',
                    name: 'editor',
                    component: Editor
                }
            ]
        },
    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // этот путь требует авторизации, проверяем залогинен ли
        // пользователь, и если нет, перенаправляем на страницу логина
        if (!store.getters.isAuthenticated) {
            next({
                path: '/login',
                query: {redirect: to.fullPath}
            })
        }
    }

    next()
});

export default router;
