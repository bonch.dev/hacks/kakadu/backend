<html>
<head>
    <title>SPA</title>
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet"/>
    <link rel="shortcut icon" href="​/images/ico/favicon.ico" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="{{ asset('js/app.js') }}"></script>
</head>
<body>
<div id="app">
    <App></App>
</div>
<script>
    new Vue({
        el: '#app',
    })
</script>
</body>
</html>
