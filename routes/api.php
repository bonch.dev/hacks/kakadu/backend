<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::any('login', 'AuthController@login');
Route::any('logout', 'AuthController@logout');

Route::middleware('auth:api')->group(function () {
    Route::apiResource('users', 'UserController');
    Route::apiResource('test', 'Test\TestController');
});